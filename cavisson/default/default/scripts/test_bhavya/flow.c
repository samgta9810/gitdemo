/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 11/28/2021 11:54:19
    Flow details:
    Build details: 4.6.1 (build# 84)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.58:9013/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://10.10.30.58:9013/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.58:9013/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.58:9013/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.58:9013/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(6.41);

    //Page Auto split for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.58:9013/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=asdfghjk&password=cvbnm&login.x=44&login.y=21&JSFormSubmit=off",
        BODY_BEGIN,
            "{"id":"remoteLogMutation","query":"mutation remoteLogMutation(\n  $input: LogInput!\n) {\n  log(input: $input) {\n    status\n  }\n}\n","variables":{"input":{"message":"REMOTE LOGGER MIDDLEWARE ::: DEBUG ::: Fri Oct 08 2021 09:43:09 GMT-0400 (Eastern Daylight Time) :::  \"Action markdown/setCreateTicketingFilters with Payload {\\\"effectiveWeek\\\":{\\\"from\\\":\\\" {pEff_startdate}\\\",\\\"to\\\":\\\"{pEff_enddate}\\\"},\\\"selectedGmm\\\":[{Gmmid}],\\\"selectedDivision\\\":[{Divisionid}],\\\"selectedDepartment\\\":[],\\\"selectedVendor\\\":[],\\\"loadOnlyVendors\\\":false} at 09:43:09.039 in 0.00 ms\"","timestamp":"1633700589040","severity":"DEBUG","halData":{"division":{Division},"location":{Locatin},"storeNumber":{StoreNum},"deviceName":"F1174130-0D4B-4174-BB66-5809F40CE3C4","deviceType":"CT40"},"clientMutationId":"55"}}}",
        BODY_END
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(1.683);

    //Page Auto split for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.58:9013/cgi-bin/reservation",
        BODY_BEGIN,
            "{"id":"remoteLogMutation","query":"mutation remoteLogMutation(\n  $input: LogInput!\n) {\n  log(input: $input) {\n    status\n  }\n}\n","variables":{"input":{"message":"REMOTE LOGGER MIDDLEWARE ::: DEBUG ::: Fri Oct 08 2021 09:43:09 GMT-0400 (Eastern Daylight Time) :::  \"Action markdown/setCreateTicketingFilters with Payload {\\\"effectiveWeek\\\":{\\\"from\\\":\\\" {pEff_startdate}\\\",\\\"to\\\":\\\"{pEff_enddate}\\\"},\\\"selectedGmm\\\":[{Gmmid}],\\\"selectedDivision\\\":[{Divisionid}],\\\"selectedDepartment\\\":[],\\\"selectedVendor\\\":[],\\\"loadOnlyVendors\\\":false} at 09:43:09.039 in 0.00 ms\"","timestamp":"1633700589040","severity":"DEBUG","halData":{"division":{Division},"location":{Locatin},"storeNumber":{StoreNum},"deviceName":"F1174130-0D4B-4174-BB66-5809F40CE3C4","deviceType":"CT40"},"clientMutationId":"55"}}}",
        BODY_END
  
        );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    ns_page_think_time(4.452);

}
